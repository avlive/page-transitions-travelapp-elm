This is an Elm port
of <https://github.com/sdras/page-transitions-travelapp>,
a Vue.js demo app from Sarah Drasner's talk
["The Future of Web Animation"](https://www.youtube.com/watch?v=UOSrrvI-Bp0)
and the related article
["Native-Like Animations for Page Transitions on the Web"](https://css-tricks.com/native-like-animations-for-page-transitions-on-the-web/).

This Elm version uses [elm-ui](https://www.youtube.com/watch?v=Ie-gqwSHQr0) and [elm-animator](https://www.youtube.com/watch?v=Nf4rElfA8SE),
and implements the [FLIP animation technique](https://css-tricks.com/animating-layouts-with-the-flip-technique/) in Elm.

The live demo is deployed at <https://avlive.gitlab.io/page-transitions-travelapp-elm/>

The development of this port was streamed live at <https://twitch.tv/avh4>.
- [Part 1](https://www.twitch.tv/videos/626197935)

## TODO

- [X] create the headers for two pages
- [X] animate the header image
- [X] animate the profile image with FLIP
- [x] generalize the FLIP setup and apply it to the Follow button
- [ ] implement the rest of the layout
- [ ] animate border radius
- [ ] set up blogger-specific pages
- [ ] convert the app to a SPA (with elm-spa)
- [ ] let caller decide how to animate in new elements
- [ ] add animation for outgoing (removed) elements
- [ ] make the layout responsive 

## Running locally

```sh
npm install
npm start
```
