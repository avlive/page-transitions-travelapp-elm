module Animator.FLIP exposing
    ( AllMeasurements
    , Timeline
    , applyAnimation
    , elmUiAttributes
    , init
    , initForHiddenMeasurement
    , measureForAnimation
    , toBasicTimeline
    , watchingWith
    )

import Animator exposing (Animator)
import Browser.Dom
import Dict exposing (Dict)
import Element exposing (..)
import Html.Attributes
import Task exposing (Task)


type Timeline state
    = Timeline
        { basicTimeline : Animator.Timeline state
        , idPrefix : String -- TODO: turn into a union type.  will be empty string for normal rendering, and "new:" for the invisible measurement frame
        , measurements : Dict String Measurement
        }


init : state -> Timeline state
init initialState =
    Timeline
        { basicTimeline = Animator.init initialState
        , idPrefix = ""
        , measurements = Dict.empty
        }


initForHiddenMeasurement : state -> Timeline state
initForHiddenMeasurement initialState =
    Timeline
        { basicTimeline = Animator.init initialState
        , idPrefix = "new:"
        , measurements = Dict.empty
        }


toBasicTimeline : Timeline state -> Animator.Timeline state
toBasicTimeline (Timeline timeline) =
    timeline.basicTimeline


type AllMeasurements
    = AllMeasurements (Dict String Measurement)


applyAnimation :
    AllMeasurements
    -> (Animator.Timeline state -> Animator.Timeline state)
    -> Timeline state
    -> Timeline state
applyAnimation (AllMeasurements updatedMeasurements) applyToBasicTimeline (Timeline timeline) =
    Timeline
        { timeline
            | basicTimeline = applyToBasicTimeline timeline.basicTimeline
            , measurements = updatedMeasurements
        }


measureForAnimation : List String -> (AllMeasurements -> msg) -> Cmd msg
measureForAnimation elementIdsToMeasure onResult =
    let
        measurementTask : String -> Task never (Maybe ( String, Measurement ))
        measurementTask elementId =
            Task.map2 Measurement
                (Browser.Dom.getElement elementId)
                (Browser.Dom.getElement ("new:" ++ elementId))
                |> Task.map (\measurement -> Just ( elementId, measurement ))
                |> Task.onError (\_ -> Task.succeed Nothing)
    in
    List.map measurementTask elementIdsToMeasure
        |> Task.sequence
        |> Task.map (List.filterMap identity)
        |> Task.perform (onResult << AllMeasurements << Dict.fromList)


type alias Measurement =
    { old : Browser.Dom.Element
    , new : Browser.Dom.Element
    }


{-| TODO: have some kind of Animator.FLIP.Timeline that encapsulates the idPrefix and the Measurement values
-}
elmUiAttributes : String -> Timeline timelineValue -> List (Attribute msg)
elmUiAttributes elementId (Timeline timeline) =
    let
        maybeMeasurement =
            Dict.get elementId timeline.measurements

        scaleValue =
            -- TODO: scale X and Y independently
            Animator.move timeline.basicTimeline <|
                \timelineValue ->
                    if timelineValue == Animator.current timeline.basicTimeline then
                        Animator.at 1

                    else
                        case maybeMeasurement of
                            Nothing ->
                                Animator.at 0

                            Just measurement ->
                                Animator.at
                                    (measurement.old.element.width / measurement.new.element.width)

        moveValue =
            Animator.xy timeline.basicTimeline <|
                \timelineValue ->
                    if timelineValue == Animator.current timeline.basicTimeline then
                        { x = Animator.at 0
                        , y = Animator.at 0
                        }

                    else
                        case maybeMeasurement of
                            Just measurement ->
                                { x =
                                    Animator.at
                                        ((measurement.new.element.x - measurement.old.element.x)
                                            + (measurement.new.element.width - measurement.old.element.width)
                                            / 2
                                        )
                                , y =
                                    Animator.at
                                        ((measurement.new.element.y - measurement.old.element.y)
                                            + (measurement.new.element.height - measurement.old.element.height)
                                            / 2
                                        )
                                }

                            Nothing ->
                                { x = Animator.at 0
                                , y = Animator.at 0
                                }
    in
    [ moveUp moveValue.y
    , moveLeft moveValue.x
    , scale scaleValue
    , htmlAttribute (Html.Attributes.id <| timeline.idPrefix ++ elementId)
    ]



-- Stuff we need to wrap from Animator for now


watchingWith :
    (model -> Timeline state)
    -> (Timeline state -> model -> model)
    -> (state -> Bool)
    -> Animator model
    -> Animator model
watchingWith get set eventIsRestable animator =
    let
        basicSet : Animator.Timeline state -> model -> model
        basicSet newBasicTimeline model =
            let
                (Timeline originalFancyTimeline) =
                    get model

                newFancyTimeline =
                    Timeline
                        { originalFancyTimeline
                            | basicTimeline = newBasicTimeline
                        }
            in
            set newFancyTimeline model
    in
    Animator.watchingWith
        (get >> toBasicTimeline)
        basicSet
        eventIsRestable
        animator
