module Blogger exposing (..)


type alias Blogger =
    { name : String
    , profileImage : String
    }


bloggers : List Blogger
bloggers =
    [ Blogger "Sophia" "profile2.jpg"
    , Blogger "Ben" "profile3.jpg"
    , jill
    , Blogger "Cynthia" "profile5.jpg"
    ]


jill =
    Blogger "Jill" "profile4.jpg"
