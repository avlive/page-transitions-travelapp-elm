module Main exposing (main)

import Animator
import Animator.FLIP
import Blogger exposing (Blogger)
import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html.Attributes
import Time


type Page
    = Home
    | Places
    | Trips


type alias Flags =
    ()


type alias Model =
    { page : Animator.FLIP.Timeline Page
    , pageForMeasuring : Maybe Page
    }


initialModel : Model
initialModel =
    { page = Animator.FLIP.init Trips
    , pageForMeasuring = Nothing
    }


main =
    Browser.document
        { init = \() -> ( initialModel, Cmd.none )
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ animator
            |> Animator.toSubscription Tick model
        ]


animator : Animator.Animator Model
animator =
    Animator.animator
        |> Animator.FLIP.watchingWith .page
            (\newPage model ->
                { model | page = newPage }
            )
            (\_ -> False)


type Msg
    = GoToPage Page
    | ElementsMeasured Page Animator.FLIP.AllMeasurements
    | Tick Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GoToPage newPage ->
            ( { model
                | pageForMeasuring = Just newPage
              }
            , Animator.FLIP.measureForAnimation
                (List.concat
                    [ [ "follow-button"
                      , "blogger-name"
                      ]
                    , List.map (\b -> "profile-image-" ++ b.profileImage) Blogger.bloggers
                    ]
                )
                (ElementsMeasured newPage)
            )

        ElementsMeasured newPage allMeasurements ->
            ( { model
                | page =
                    model.page
                        |> Animator.FLIP.applyAnimation allMeasurements
                            (Animator.go (Animator.millis 900) newPage)
                , pageForMeasuring = Nothing
              }
            , Cmd.none
            )

        Tick newTime ->
            ( Animator.update newTime animator model
            , Cmd.none
            )


view : Model -> Browser.Document Msg
view model =
    { title = "Travel app demo"
    , body =
        [ layout
            [ width fill
            , height fill
            , behindContent <|
                case model.pageForMeasuring of
                    Nothing ->
                        none

                    Just pageForMeasuring ->
                        el
                            [ width fill
                            , htmlAttribute (Html.Attributes.style "visibility" "hidden")
                            ]
                            (viewPage
                                (Animator.FLIP.initForHiddenMeasurement pageForMeasuring)
                            )
            ]
            (viewPage model.page)
        ]
    }


viewPage : Animator.FLIP.Timeline Page -> Element Msg
viewPage page =
    case Animator.current (Animator.FLIP.toBasicTimeline page) of
        Home ->
            viewHome page

        Places ->
            viewPlaces page

        Trips ->
            viewTrips page


headerImageHeight =
    300


headerImage : Page -> String
headerImage page =
    case page of
        Home ->
            "header1.jpg"

        Places ->
            "header2.jpg"

        Trips ->
            "header3.jpg"


backgroundImage : Animator.Timeline Page -> Element msg
backgroundImage timeline =
    let
        alphaValue =
            Animator.move timeline <|
                \page ->
                    if page == Animator.current timeline then
                        Animator.at 0

                    else
                        Animator.at 1

        previous =
            el
                [ width fill
                , height fill
                , Background.image (headerImage <| Animator.previous timeline)
                , alpha alphaValue
                ]
                none
    in
    el
        [ width fill
        , height (px headerImageHeight)
        , Background.image (headerImage <| Animator.current timeline)
        , inFront previous
        ]
        none


viewHome : Animator.FLIP.Timeline Page -> Element Msg
viewHome timeline =
    el
        [ behindContent (backgroundImage (Animator.FLIP.toBasicTimeline timeline))
        , width fill
        ]
        (column
            [ width (px 1000)
            , centerX
            ]
            [ navBar
            , homeHeader timeline
            , let
                alphaValue =
                    Animator.move (Animator.FLIP.toBasicTimeline timeline) <|
                        \page ->
                            case page of
                                Home ->
                                    Animator.at 1

                                _ ->
                                    Animator.at 0
              in
              el
                [ paddingXY 0 30
                , width fill
                , alpha alphaValue
                ]
                (placeholder
                    [ width fill
                    , height (px 400)
                    , Background.color (rgb 0.0941 0.2941 0.0392)
                    ]
                    "body content"
                )
            ]
        )


navBarHeight : Int
navBarHeight =
    50


navBar : Element Msg
navBar =
    let
        viewNavButton ( linkText, page ) =
            Input.button
                [ Font.color (rgb 1 1 1)
                ]
                { onPress = Just (GoToPage page)
                , label = text linkText
                }
    in
    row
        [ height (px navBarHeight)
        , width fill
        , Background.color (rgba 0 0 0 0.4)
        ]
        [ row
            [ alignLeft
            , spacing 60
            ]
            (List.map viewNavButton
                [ ( "Jill's Home", Home )
                , ( "Jill's Places", Places )
                , ( "Jill's Group Trips", Trips )
                ]
            )
        , placeholder [ alignRight ] "... menu"
        ]


homeHeader :
    Animator.FLIP.Timeline Page
    -> Element msg
homeHeader timeline =
    row
        [ height (px <| headerImageHeight + 120 - navBarHeight)
        , width fill
        , spacing 30
        ]
        [ column
            [ spacing 15
            ]
            [ row
                [ spacing 20
                , height (px 280)
                ]
                [ viewProfileImage
                    [ alignBottom
                    ]
                    { size = 200
                    , borderRadius = 5 / 200
                    }
                    timeline
                    Blogger.jill
                , el
                    ([ Background.color (rgb 1 0.27 0)
                     , padding 10
                     , Font.color (rgb 1 1 1)
                     , Border.rounded 5
                     , alignBottom
                     , width (px 150)
                     , Font.center
                     ]
                        ++ Animator.FLIP.elmUiAttributes "follow-button" timeline
                    )
                    (text "Follow")
                ]
            , el
                ([ Font.size 35 ]
                    ++ Animator.FLIP.elmUiAttributes "blogger-name" timeline
                )
                (text "Jill Fernandez")
            ]
        , column
            [ alignRight
            , width fill
            , alignBottom
            ]
            [ placeholder
                [ width fill
                ]
                "empty space with Mail button"
            , row
                [ height (px 120)
                ]
                [ placeholder [] "bio"
                , placeholder [] "follower count"
                , placeholder [] "following count"
                ]
            ]
        ]


viewProfileImage :
    List (Element.Attribute msg)
    ->
        { size : Int
        , borderRadius : Float
        }
    -> Animator.FLIP.Timeline Page
    -> Blogger
    -> Element msg
viewProfileImage attrs config timeline blogger =
    image
        ([ Border.rounded (round (toFloat config.size * config.borderRadius))
         , width (px config.size)
         , height (px config.size)
         , clip
         ]
            ++ attrs
            ++ Animator.FLIP.elmUiAttributes ("profile-image-" ++ blogger.profileImage) timeline
        )
        { src = blogger.profileImage
        , description = "Photo of " ++ blogger.name
        }


viewPlaces : Animator.FLIP.Timeline Page -> Element Msg
viewPlaces timeline =
    el
        [ behindContent (backgroundImage (Animator.FLIP.toBasicTimeline timeline))
        , width fill
        ]
        (column
            [ width (px 1000)
            , centerX
            ]
            [ navBar
            , placesHeader timeline
            , let
                alphaValue =
                    Animator.move (Animator.FLIP.toBasicTimeline timeline) <|
                        \page ->
                            case page of
                                Places ->
                                    Animator.at 1

                                _ ->
                                    Animator.at 0
              in
              el
                [ paddingXY 0 30
                , width fill
                , alpha alphaValue
                ]
                (placeholder
                    [ width fill
                    , height (px 400)
                    , Background.color (rgb 0.1216 0.6275 0.6784)
                    ]
                    "body content"
                )
            ]
        )


placesHeader :
    Animator.FLIP.Timeline Page
    -> Element msg
placesHeader timeline =
    row
        [ height (px <| headerImageHeight - navBarHeight)
        , width fill
        , spacing 30
        ]
        [ column
            [ spacing 15
            ]
            [ row
                [ spacing 20
                ]
                [ viewProfileImage
                    [ alignBottom
                    ]
                    { size = 150
                    , borderRadius = 5 / 150
                    }
                    timeline
                    Blogger.jill
                ]
            , row
                [ spacing 10
                ]
                [ el
                    ([ Background.color (rgb 1 0.27 0)
                     , padding 10
                     , Font.color (rgb 1 1 1)
                     , Border.rounded 5
                     , alignBottom
                     , width (px 150)
                     , Font.center
                     ]
                        ++ Animator.FLIP.elmUiAttributes "follow-button" timeline
                    )
                    (text "Follow")
                , el
                    ([ Font.size 26
                     , Font.color (rgb 1 1 1)
                     ]
                        ++ Animator.FLIP.elmUiAttributes "blogger-name" timeline
                    )
                    (text "Jill Fernandez")
                ]
            ]
        , column
            [ alignRight
            , width fill
            , alignBottom
            ]
            [ placeholder
                [ width fill
                ]
                "empty space with Plus button"
            , placeholder [ alignRight ] "days remaining"
            ]
        ]


viewTrips : Animator.FLIP.Timeline Page -> Element Msg
viewTrips timeline =
    el
        [ behindContent (backgroundImage (Animator.FLIP.toBasicTimeline timeline))
        , width fill
        ]
        (column
            [ width (px 1000)
            , centerX
            ]
            [ navBar
            , tripsHeader timeline
            , let
                alphaValue =
                    Animator.move (Animator.FLIP.toBasicTimeline timeline) <|
                        \page ->
                            case page of
                                Trips ->
                                    Animator.at 1

                                _ ->
                                    Animator.at 0
              in
              el
                [ paddingXY 0 30
                , width fill
                , alpha alphaValue
                ]
                (placeholder
                    [ width fill
                    , height (px 400)
                    , Background.color (rgb 0.1216 0.6275 0.6784)
                    ]
                    "body content"
                )
            ]
        )


tripsHeader :
    Animator.FLIP.Timeline Page
    -> Element msg
tripsHeader timeline =
    let
        profileBorderRadius =
            Animator.move (Animator.FLIP.toBasicTimeline timeline) <|
                \page ->
                    case page of
                        Trips ->
                            Animator.at 0.5

                        _ ->
                            Animator.at 0

        viewIcon blogger =
            el
                [ inFront <|
                    el
                        [ width (px 10)
                        , height (px 10)
                        , Background.color (rgb255 0x07 0xDC 0x3C)
                        , Border.rounded 5
                        , Border.color (rgb 0 0 0)
                        , Border.width 1
                        , alignBottom
                        , alignRight
                        ]
                        none
                ]
            <|
                viewProfileImage
                    []
                    { size = 50
                    , borderRadius = profileBorderRadius
                    }
                    timeline
                    blogger
    in
    row
        [ height (px <| headerImageHeight - navBarHeight)
        , width fill
        , spacing 30
        , paddingXY 0 30
        ]
        [ column
            [ width fill
            , height fill
            , spacing 30
            ]
            [ row
                [ alignBottom
                , spacing 5
                , width fill
                ]
                (List.map viewIcon Blogger.bloggers)
            , el
                [ alignBottom
                , Font.color (rgb 1 1 1)
                , Font.size 35
                ]
                (text "Honolulu")
            ]
        , placeholder
            [ height fill
            ]
            "right"
        ]


wireframeTheme =
    { bg = rgb 0.9 0.9 0.9
    , frame = rgb 0.5 0.5 0.5
    , text = rgb 0.3 0.3 0.3
    }


placeholder : List (Attribute msg) -> String -> Element msg
placeholder attr name =
    text name
        |> el
            [ Border.rounded 5
            , Border.dotted
            , Border.color wireframeTheme.frame
            , Border.width 2
            , height fill
            , width fill
            , padding 20
            , Background.color wireframeTheme.bg
            , Font.center
            , Font.color wireframeTheme.text
            , alpha 0.4
            ]
        |> el attr
